// SPDX-FileCopyrightText: 2021 Carson Black <uhhadd@gmail.com>
//
// SPDX-License-Identifier: MIT

package main

import (
	"bufio"
	"bytes"
	"i18n-extractor/log"
	"io/ioutil"
	"os"
	"path"
	"strings"

	"github.com/urfave/cli/v2"
)

func isEqual(old, new string) (conflictingOld, conflictigNew string, ok bool) {
	oldFile, err := os.Open(old)
	if err != nil {
		if os.IsNotExist(err) {
			return "does not exist", "", false
		}
		log.Fatal(2, "%+v", err)
	}

	newFile, err := os.Open(new)
	if err != nil {
		log.Fatal(2, "%+v", err)
	}

	oldScanner := bufio.NewScanner(oldFile)
	newScanner := bufio.NewScanner(newFile)

	for oldScanner.Scan() {
		newScanner.Scan()

		oldStr := string(oldScanner.Bytes())
		newStr := string(newScanner.Bytes())

		if strings.HasPrefix(oldStr, "#") && strings.HasPrefix(newStr, "#") {
			continue
		}

		if strings.Contains(oldStr, "POT-Creation-Date") && strings.Contains(newStr, "POT-Creation-Date") {
			continue
		}

		if !bytes.Equal(oldScanner.Bytes(), newScanner.Bytes()) {
			return oldStr, newStr, false
		}
	}

	return "", "", true
}

// Validate ensures that there are no changes that need to be made
func Validate(c *cli.Context) error {
	tmpdir, err := ioutil.TempDir("/tmp", "*")
	if err != nil {
		return err
	}

	proj, err := Run(c, tmpdir)
	if err != nil {
		return err
	}

	for _, sub := range proj.Translations {
		oldFile := path.Join(reporoot, proj.Outdir, sub.Name+".pot")
		newFile := path.Join(tmpdir, proj.Outdir, sub.Name+".pot")

		if oldstr, newstr, ok := isEqual(oldFile, newFile); !ok {
			log.Fatal(3, "The translations for %s are not up to date. Please run 'i18n-extractor' run to update them.\n\tOld: %s\n\tNew: %s", sub.Name, oldstr, newstr)
		}
	}

	log.Info("Translations validated.")

	return nil
}
