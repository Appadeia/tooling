// SPDX-FileCopyrightText: 2021 Carson Black <uhhadd@gmail.com>
//
// SPDX-License-Identifier: MIT

package main

import (
	"bytes"
	"fmt"
	"i18n-extractor/log"
	"io/ioutil"
	"os"
	"os/exec"
	"path"

	"github.com/urfave/cli/v2"
	"gopkg.in/yaml.v2"
)

// Run runs the app
func Run(c *cli.Context, alternateRoot string) (*Project, error) {
	log.Info("Reading messages.yaml...")
	data, err := ioutil.ReadFile("messages.yaml")
	if err != nil {
		return nil, fmt.Errorf("error reading messages.yaml: %w", err)
	}

	var proj Project
	err = yaml.Unmarshal(data, &proj)
	if err != nil {
		return nil, fmt.Errorf("error parsing messages.yaml: %w", err)
	}

	outdir := path.Join(reporoot, proj.Outdir)
	if alternateRoot != "" {
		outdir = path.Join(alternateRoot, proj.Outdir)
	}

	err = os.MkdirAll(path.Join(outdir), os.ModePerm)
	if err != nil {
		return nil, fmt.Errorf("error creating outdir: %w", err)
	}

	for idx, sub := range proj.Translations {
		log.Info("[%d/%d] Doing translations for %s...", idx+1, len(proj.Translations), sub.Name)
		outfile := path.Join(outdir, sub.Name+".pot")

		inputs, err := globMany(sub.Inputs)
		if err != nil {
			return nil, fmt.Errorf("error finding input files: %w", err)
		}

		if len(sub.ExtractRC) > 0 {
			tmpfile, err := ioutil.TempFile("/tmp", "*.cpp")
			defer tmpfile.Close()
			if err != nil {
				return nil, fmt.Errorf("error creating tempfile for extractrc: %w", err)
			}

			globs, err := globMany(sub.ExtractRC)
			if err != nil {
				return nil, fmt.Errorf("error finding input files for extractrc: %w", err)
			}

			log.Info("Running extractrc...")
			extr := exec.Command("extractrc", globs...)
			extr.Stdout = tmpfile
			err = extr.Run()
			if err != nil {
				return nil, fmt.Errorf("error running extractrc: %w", err)
			}

			inputs = append(inputs, tmpfile.Name())
		}

		inputs = append(xgettextArgs, inputs...)
		inputs = append(inputs, "-o", outfile)

		log.Info("Running xgettext...")
		xgettext := exec.Command("xgettext", inputs...)
		stderr := bytes.NewBuffer(nil)
		xgettext.Stderr = stderr

		err = xgettext.Run()
		if err != nil {
			return nil, fmt.Errorf("error running xgettext: %w\n%s", err, stderr.String())
		}
	}

	return &proj, nil
}
