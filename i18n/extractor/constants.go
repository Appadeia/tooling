// SPDX-FileCopyrightText: 2021 Carson Black <uhhadd@gmail.com>
//
// SPDX-License-Identifier: MIT

package main

import (
	"log"
	"path/filepath"
)

var (
	xgettextArgs = []string{
		"-F",
		"--from-code=UTF-8",
		"-C",
		"--kde",
		"-ci18n",
		"-ki18n:1",
		"-ki18nc:1c,2",
		"-ki18np:1,2",
		"-ki18ncp:1c,2,3",
		"-ktr2i18n:1",
		"-kI18N_NOOP:1",
		"-kI18N_NOOP2:1c,2",
		"-kI18N_NOOP2_NOSTRIP:1c,2",
		"-kaliasLocale",
		"-kki18n:1",
		"-kki18nc:1c,2",
		"-kki18np:1,2",
		"-kki18ncp:1c,2,3",
	}
	reporoot = ""
)

func init() {
	out, err := filepath.Abs("messages.yaml")
	if err != nil {
		log.Fatalf("%+v", err)
	}

	reporoot = filepath.Dir(out)
}
