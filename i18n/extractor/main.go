// SPDX-FileCopyrightText: 2021 Carson Black <uhhadd@gmail.com>
//
// SPDX-License-Identifier: MIT

package main

import (
	"os"
	"path/filepath"

	"i18n-extractor/log"

	"github.com/urfave/cli/v2"
)

// Project contains the deserialised form of the project
type Project struct {
	Outdir       string `yaml:"outdir"`
	Translations []struct {
		Name      string   `yaml:"name"`
		Root      string   `yaml:"root"`
		Inputs    []string `yaml:"inputs"`
		ExtractRC []string `yaml:"extractrc"`
	} `yaml:"translations"`
}

func globMany(s []string) (ret []string, err error) {
	var matches []string

	for _, g := range s {
		matches, err = filepath.Glob(g)
		if err != nil {
			return
		}

		ret = append(ret, matches...)
	}

	return
}

func main() {
	app := cli.App{
		Name:  "kwinft i18n extractor",
		Usage: "Extract strings from source files into .pot files",
		ExitErrHandler: func(context *cli.Context, err error) {
			log.Fatal(1, "error running translations: %+v", err)
		},
		Commands: []*cli.Command{
			{
				Name: "run",
				Action: func(c *cli.Context) error {
					_, err := Run(c, "")
					return err
				},
			},
			{
				Name:   "validate",
				Action: Validate,
			},
		},
	}
	app.Run(os.Args)
}
